// src/calc.js
/**
* Adds two numbers together
* @param {number} a
* @param {number} b
* @returns {number}
*/
const add = (a, b) => a + b;
/**
* Subtracts number from minuend
* @param {number} minuend
* @param {number} subtrahend
* @returns {number} difference
*/
const subtract = (minuend, subtrahend) => {
return minuend - subtrahend;
}

/**
 * Multiplies two numbers together
 * @param {number} a 
 * @param {number} b 
 * @returns {number} product
 */
const multiply = (a, b) => a * b;

/**
 * Divides the first number with the second number
 * @param {number} a number to divide
 * @param {number} b divisor
 * @returns {number} result
 */
const divide = function(a, b) {
    if (b == 0) {
        throw new Error('Cannot divide by zero');
    }
    return a / b;
}

export default { add, subtract, multiply, divide }