import express from 'express';
import calc from './calc.js';

const app = express();
const port = 3000;
const host = "localhost";

app.get('/', (req, res) => {
    res.status(200).send("Hello world!");
});

/**
 * Endpoint for add operation
 */
app.get('/add', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const result = calc.add(a, b);
    res.status(200).send(result.toString())
})

/**
 * Endpoint for subtract operation
 */
app.get('/subtract', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const result = calc.subtract(a, b);
    res.status(200).send(result.toString())
})

/**
 * Endpoint for multiplication operation
 */
app.get('/multiply', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const result = calc.multiply(a, b);
    res.status(200).send(result.toString())
})

/**
 * Endpoint for division operation
 */
app.get('/divide', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const result = calc.divide(a, b);
    res.status(200).send(result.toString())
})

app.listen(port, host, () => {
    console.log(`http://${host}:${port}`);
});

