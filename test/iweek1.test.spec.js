import { expect, should, assert } from "../node_modules/chai/chai.js";
import calc from "../src/calc.js";

describe("extra tests set 1", () => {
    before(() => { should(); console.log("  starting testing...");});

    describe("add", () => {
        it("can add two positive numbers together", () => {
            expect(calc.add(2, 2)).to.equal(4);
        });
        it("can add two negative numbers together", () => {
            expect(calc.add(-2, -2)).to.equal(-4);
        });
        it("can add zeros", () => {
            expect(calc.add(0, 0)).to.equal(0);
        });
    });

    describe("subtract", () => {
        it("can subtract two positive numbers", () => {
            assert.equal(calc.subtract(5, 1), 4)
        });
        it("can subtract negative number", () => {
            assert.equal(calc.subtract(5, -3), 8)
        });
    });

    describe("multiply", () => {
        it("can multiply two positive numbers", function() {
            calc.multiply(3, 3).should.equal(9);
        });
        it("can multiply two negative numbers", function() {
            calc.multiply(-3, -3).should.equal(9);
        });
        it("can multiply one negative and one positive", function() {
            calc.multiply(-3, 3).should.equal(-9);
        });
        it("can multiply with zero", function() {
            calc.multiply(0, 3).should.equal(0);
        });
    });

    describe("divide", () => {
        it("can divide with positive number", () => {
            expect(calc.divide(6, 2)).to.equal(3);
        });
        it("can divide with negative number", () => {
            expect(calc.divide(6, -2)).to.equal(-3);
        });
        it("cannot divide by zero", () => {
            expect(() => calc.divide(3, 0)).to.throw('Cannot divide by zero');
        });
    });

    after(() => console.log("  ...all tests done!"));
});

