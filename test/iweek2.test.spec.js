import { expect } from "../node_modules/chai/chai.js";
import calc from "../src/calc.js";

describe("second set of tests for iweek", () => {
    it("can add numbers", () => {
        expect(calc.add(2, 2)).to.equal(4);
    });
    it("can add numbers", () => {
        expect(calc.add(0, 0)).to.equal(0);
    });
    it("can subtract numbers", () => {
        expect(calc.subtract(5, 1)).to.equal(4);
    });
    it("can subtract numbers", () => {
        expect(calc.subtract(1, 5)).to.equal(-4);
    });
    it("can multiply numbers", () => {
        expect(calc.multiply(3, 3)).to.equal(9);
    });
    it("can multiply numbers", () => {
        expect(calc.multiply(0, 5)).to.equal(0);
    });
    it("can divide numbers", () => {
        expect(calc.divide(6, 2)).to.equal(3);
    });
    it("cannot divide by zero", () => {
        expect(() => calc.divide(0, 0)).to.throw();
    });
    it("cannot divide by zero", () => {
        expect(calc.divide(5, 1)).to.equal(5);
    });
});

